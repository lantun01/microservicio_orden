import { MigrationInterface, QueryRunner } from "typeorm";

export class Initial1701621620010 implements MigrationInterface {
    name = 'Initial1701621620010'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "orders" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "idUser" integer NOT NULL, "idProducto" integer NOT NULL, "total" integer NOT NULL, "orderAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_710e2d4957aa5878dfe94e4ac2f" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "orders"`);
    }

}
