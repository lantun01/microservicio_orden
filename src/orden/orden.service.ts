import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOrdenDto } from './dto/create-orden.dto';
import { UpdateOrdenDto } from './dto/update-orden.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Orden } from './entities/orden.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OrdenService {

  constructor(@InjectRepository(Orden)private readonly OrdenRepository:Repository<Orden>){}


  async create(createOrdenDto: CreateOrdenDto):Promise<Orden>{
    const product= this.OrdenRepository.create(createOrdenDto);
    return await this.OrdenRepository.save(product);
  }

  async findAll():Promise<Orden[]> {
    return await this.OrdenRepository.find();
  }

  async findOne(id: number) {
    const orden= await this.OrdenRepository.findOne({
      where: {id:id}
    });

    if(!orden) throw new NotFoundException('orden no encontrado');
    return orden;
  }

  async update(updateOrdenDto: Partial<UpdateOrdenDto>):Promise<Orden> {
    const orden =await this.findOne(updateOrdenDto.id);
    Object.assign(orden,updateOrdenDto)
    return await this.OrdenRepository.save(orden);
  }
  
}
