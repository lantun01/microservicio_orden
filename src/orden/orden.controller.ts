import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { OrdenService } from './orden.service';
import { CreateOrdenDto } from './dto/create-orden.dto';
import { UpdateOrdenDto } from './dto/update-orden.dto';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';

@Controller('orden')
export class OrdenController {
  constructor(private readonly ordenService: OrdenService) {}

  @MessagePattern('Create_Orden')
  Create(@Payload() create: CreateOrdenDto) {
    return this.ordenService.create(create);
  }

  @MessagePattern('findAllOrden')
  find() {
    return this.ordenService.findAll();
  }

  @MessagePattern('findOneOrder')
  findOne(@Payload() id: number) {
    return this.ordenService.findOne(id);
  }

  @MessagePattern('OrderUpdate')
  update(@Payload() update:  UpdateOrdenDto) {
    return this.ordenService.update(update);
  }

  
//////////////////////////////////////////////////////////////////////////
  @MessagePattern({cmd: 'greeting'})
  getGreetingMessage(name: string): string {
    return `Hello ${name}`;
  }

  @MessagePattern({cmd: 'greeting-async'})
  async getGreetingMessageAysnc(name: string): Promise<string> {
    return `Hello ${name} Async`;
  }

  @EventPattern('book-created')
  async handleBookCreatedEvent(data: Record<string, unknown>) {
    console.log(data);
  }
}
