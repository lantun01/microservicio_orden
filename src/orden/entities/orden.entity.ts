import { UserEntity } from "src/entitis/user.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, Timestamp } from "typeorm";


@Entity({name:'orders'})
export class Orden {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    title:string;

    @Column()
    description:string;

    @Column()
    idUser:number;

    @Column()
    idProducto:number;

    @Column()
    total:number;

    //fecha de orden
    @CreateDateColumn()
    orderAt:Timestamp;

    @ManyToOne(() => UserEntity, usuario => usuario.lista)
    usuario: UserEntity;

}
