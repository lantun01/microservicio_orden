import { IsNotEmpty, IsNumber, IsPositive, IsString, Min } from "class-validator";

export class CreateOrdenDto {
    @IsNotEmpty({message:'el titulo no puede estar vacio'})
    @IsString()
    title:string;

    @IsNotEmpty({message:'la descripcion no puede estar vacia'})
    @IsString()
    description:string;

    @IsNotEmpty({message:'el precio no puede ser cero'})
    @IsNumber({maxDecimalPlaces:2},{message:'el precio debe ser un numero con no mas de 2 decimales'})
    @IsPositive({message:'el precio es un numero positivo'})
    idUser:number;

    @IsNotEmpty({message:'el stock no puede estar vacio'})
    @IsNumber({},{message:'el stock debe ser un numero'})
    @Min(0,{message:'no puede ser negativo'})
    idProducto:number;

    total:number;
}
