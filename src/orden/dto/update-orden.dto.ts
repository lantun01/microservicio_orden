import { PartialType } from '@nestjs/mapped-types';
import { CreateOrdenDto } from './create-orden.dto';
import { IsNotEmpty } from 'class-validator';

export class UpdateOrdenDto extends PartialType(CreateOrdenDto) {
  @IsNotEmpty()
  id: number;
}
