import { Module } from '@nestjs/common';
import { OrdenModule } from './orden/orden.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from 'db/data-source';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [OrdenModule,TypeOrmModule.forRoot(dataSourceOptions),
    JwtModule.register({
      global: true,
      secret: process.env.ACCESS_TOKEN_SECRET_KEY,
      signOptions: { expiresIn: process.env.ACCESS_TOKEN_EXPIRE_TIME },
    })],
  controllers: [],
  providers: [],
})
export class AppModule {}
